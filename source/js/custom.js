'use strict';
var mimamo = {
	width: 0,
	height: 0,
	isMobile: false,
	videos: [],
	assets: [],
	hash: '',
	init: function() {
		mimamo.assets.push('assets/img/frame_feature.png');
		$('[image-preload="true"]').each(function() {
			mimamo.assets.push($(this).attr('src'));
		});

		mimamo.getHash();
		mimamo.mobileDetect();
		// for mobile
		if (mimamo.isMobile) {
			$('html').addClass('bp-touch');
		}

		mimamo.loader('init');
		mimamo.resize();
		mimamo.inview();

		// for custom accordion
		$('[select-box]').each(function(index) {
			var _class = 'selectbox' + index;
			$(this).addClass(_class);

			mimamo.selectBox({
				target: '.' + _class
			});
		});

		mimamo.preload(mimamo.assets, function() {
			$(document).ready(function() {
				mimamo.ready();
			});
		});

		mimamo.mobile();
	},
	ready: function() {
		mimamo.resize();
		// hide the preloader
		mimamo.loader('close');

		mimamo.animation();

		mimamo.featureAnimation({
			target: '.hc-he-icons'
		});

		mimamo.presentationAnimation({
			target: '.he-co-pres'
		});

		// for popup
		$('[data-fancybox]').fancybox({
			animationEffect: 'zoom-in-out',
			clickSlide: false,
			clickOutside: false,
			autoFocus: false,
			touch: false,
			btnTpl: {
				smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}">' + '<i class="fas fa-times"></i>' +"</button>"
			},
			afterShow: function() {
				mimamo.resize();
			}
		});

		// for scrollar
		if($('.scrollar').length != 0) {
			var scrollar = new Scrollar('.scrollar');
			// if(mimamo.width > 768) {}
		}

		// for flexslider
		$('.hc-re-slider .flexslider').flexslider({
			directionNav: true,
			controlNav: false,
			prevText: '',
			nextText: '',
			animationSpeed: 400,
			slideshowSpeed: 15000
		});

		$('.ho-pr-review .owl-carousel').owlCarousel({
			items: 3,
			margin: 36,
			dots: false,
			nav: true,
			loop: false,
			margin: 15,
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 2
				},
				810: {
					items: 3,
					margin: 20
				},
				1200: {
					loop: true,
					margin: 36
				}
			}
		});

		// for one nav
		mimamo.oneNav({
			target: '#one-nav'
		});

		// for sticky content
		$('[sticky-content]').stick_in_parent({
			offset_top: ($('header').outerHeight() + 20)
		});
	},
	loader: function(state) {
		var _loader = {
			content: '',
			init: function() {
				// $('.bp-preloader').remove();
			},
			close: function() {
				$('.bp-preloader').fadeOut();
			}
		}

		if(state == 'init') {
			_loader.init();
		} else if(state == 'close') {
			_loader.close();
		}
	},
	resize: function() {
		var _resize = {
			init: function() {
				mimamo.width = $(window).outerWidth();
				mimamo.height = $(window).outerHeight();

				// STICKY FOOTER
				var headerHeight = $('header').outerHeight(),
				footerHeight = $('footer').outerHeight(),
				footerTop = (footerHeight) * -1;
				$('footer').css({marginTop: footerTop});
				$('#main-wrapper').css({paddingTop: headerHeight, paddingBottom: footerHeight});

				// for equal height
				mimamo.equalize($('.classname'));
			}
		}
		_resize.init();
	},
	equalize: function(target) {
		$(target).css({minHeight: 0});
		var _biggest = 0;
		for ( var i = 0; i < target.length ; i++ ){
			var element_height = $(target[i]).outerHeight();
			if(element_height > _biggest ) _biggest = element_height;
		}
		$(target).css({minHeight: _biggest});
		return _biggest;
	},
	getHash: function() {
		mimamo.hash = window.location.hash;
	},
	mobileDetect: function() {
		var isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};
		if(isMobile.any) {
			mimamo.isMobile = isMobile.any();
		}
	},
	preload: function(assetArray, afterLoad) {
		var _assetCount = assetArray.length,
		_assetLoaded = [];

		var _preload = {
			init: function() {
				for (var i = 0; i < assetArray.length; i++) {
					var _fileType = assetArray[i].replace(/^.*\./, '').split("?")[0];
					var _asset = assetArray[i];

					// if asset is image
					if($.inArray(_fileType, ['gif', 'png', 'jpg', 'jpeg', 'bmp']) >= 0) {
						var _image = document.createElement('img');
						$(_image).attr('src', _asset);

						_image.onload = function() {
							_assetLoaded.push(this);
							_preload.recount();
							if(mimamo.assetLog) {
								console.log(this);
							}
						}

						// if the image does not exist, let still proceed
						_image.onerror = function() {
							_assetLoaded.push(this);
							_preload.recount();
						}
					}

					// if asset is audio
					if($.inArray(_fileType, ['mp3', 'ogg']) >= 0) {
						var _audio = document.createElement('audio');
						_audio.src = _asset,
						_count = 3;

						if(mimamo.isMobile) {
							_count = 1;
						}

						var _checker = {
							run: function() {
								if (_audio.readyState < _count) {
									_assetLoaded.push(this);
									_preload.recount();
									if(mimamo.assetLog) {
										console.log(this);
									}
								} else {
									setTimeout(checkload, 100);
								}
							}
						}
						_checker.run();
					}

					// if asset is video
					if($.inArray(_fileType, ['mp4', 'wav']) >= 0) {
						var _this = $(this),
						_video = document.createElement('video'),
						_videoSrc = document.createElement('source'),
						_count = 4;

						if(mimamo.isMobile) {
							_count = 1;
						}

						_videoSrc.src = _asset;
						_videoSrc.type = 'video/' + _fileType + '';
						_video.appendChild(_videoSrc);

						var _checker = {
							run: function() {
								if (_video.readyState == _count || _video.readyState > _count) {
									_assetLoaded.push(_this);
									_preload.recount();
									
									if(mimamo.assetLog) {											
										console.log(_this, _asset, _video.readyState, 'complete');
									}
								} else {
									setTimeout(function() {
										_checker.run();

										if(mimamo.assetLog) {											
											console.log(_this, _asset, _video.readyState, 'initial');
										}
									}, 50);
								}
							}
						}

						if(!(mimamo.browser == 'explorer')) {
							_checker.run();
						} else {
							setTimeout(function() {
								_assetLoaded.push(_this);
								_preload.recount();
							}, 2000);
						}
					}
				}
			},
			recount: function() {
				if(_assetCount == _assetLoaded.length) {
					if(afterLoad) {
						afterLoad();
					}

					if(mimamo.assetLog) {
						setTimeout(function() {
							$('.asset-log').remove();
						}, 2000);
					}
				}

				if(mimamo.assetLog) {
					_preload.report();
				}
			},
			report: function(extra) {
				$('.asset-log').remove();

				var _log = document.createElement('div');
				$(_log).addClass('asset-log').css({zIndex: 99999, position: 'fixed', bottom: '0', right: '0', color: '#ffffff', fontSize: 9});
				$(_log).append(_assetLoaded.length + ' asset loaded over ' + _assetCount + ' ' + extra + ' ' + mimamo.isMobile + ' ' + mimamo.browser);
				$('html').append(_log);
			}
		}

		if(assetArray.length == 0) {
			if(afterLoad) {
				afterLoad();
			}
		} else {
			_preload.init();
		}
	},
	inview: function() {
		(function(d){var p={},e,a,h=document,i=window,f=h.documentElement,j=d.expando;d.event.special.inview={add:function(a){p[a.guid+"-"+this[j]]={data:a,$element:d(this)}},remove:function(a){try{delete p[a.guid+"-"+this[j]]}catch(d){}}};d(i).bind("scroll resize",function(){e=a=null});!f.addEventListener&&f.attachEvent&&f.attachEvent("onfocusin",function(){a=null});setInterval(function(){var k=d(),j,n=0;d.each(p,function(a,b){var c=b.data.selector,d=b.$element;k=k.add(c?d.find(c):d)});if(j=k.length){var b;
		if(!(b=e)){var g={height:i.innerHeight,width:i.innerWidth};if(!g.height&&((b=h.compatMode)||!d.support.boxModel))b="CSS1Compat"===b?f:h.body,g={height:b.clientHeight,width:b.clientWidth};b=g}e=b;for(a=a||{top:i.pageYOffset||f.scrollTop||h.body.scrollTop,left:i.pageXOffset||f.scrollLeft||h.body.scrollLeft};n<j;n++)if(d.contains(f,k[n])){b=d(k[n]);var l=b.height(),m=b.width(),c=b.offset(),g=b.data("inview");if(!a||!e)break;c.top+l>a.top&&c.top<a.top+e.height&&c.left+m>a.left&&c.left<a.left+e.width?
		(m=a.left>c.left?"right":a.left+e.width<c.left+m?"left":"both",l=a.top>c.top?"bottom":a.top+e.height<c.top+l?"top":"both",c=m+"-"+l,(!g||g!==c)&&b.data("inview",c).trigger("inview",[!0,m,l])):g&&b.data("inview",!1).trigger("inview",[!1])}}},250)})(jQuery);
	},
	animation: function() {
		$('.animate').each(function() {
			var thisAnim = $(this), // this content
			animControl = $(this).attr('anim-control'), // for single or parent container
			animName = $(this).attr('anim-name'), // customize animation
			animDelay = parseFloat($(this).attr('anim-delay')), // delay value
			delayIncrease = 0.2;
			// if the container is parent
			if(animControl == 'parent') {
				// add delay to each child element
				$(this).children().each(function(index) {
					var element = $(this);
					if(isNaN(animDelay)) {
						animDelay = 0.5;
					}
					if(animDelay == 0) {
	                    delayIncrease = 0;
	                }
					var delayNum = animDelay + (delayIncrease * index) + 's';
					setTimeout(function() {
						$(element).css('-webkit-animation-delay', delayNum)
							.css('-moz-animation-delay', delayNum)
							.css('-ms-animation-delay', delayNum)
							.css('-o-animation-delay', delayNum)
					}, 100);
				});
				$(this).css({opacity: 1});

				if(animName == null) {
					// if no customize animation then use default animation
					$(this).children().each(function(index) {
						$(this).addClass('anim-content');
					});
				} else {
					// if have customize animation
					$(this).children().each(function(index) {
						$(this).addClass('animated');
						$(this).on('inview',function(event,visible){
							if (visible == true) {
								$(this).addClass(animName);
							}
						});
					});
				}

			// if the container is not parent
			} else {
				if(animName == null) {
					// if no customize animation then use default animation
					$(this).addClass('anim-content');
				} else {
					// if have customize animation
					$(this).addClass('animated');
					$(this).on('inview',function(event,visible){
						if (visible == true) {
							$(this).addClass(animName);
						}
					});
				}
				// if have customize animation-delay
				if(animDelay != null) {
					if(isNaN(animDelay)) {
						animDelay = 0.5;
					}
					var delayNum = animDelay + 's';
					$(this).css('-webkit-animation-delay', delayNum)
							.css('-moz-animation-delay', delayNum)
							.css('-ms-animation-delay', delayNum)
							.css('-o-animation-delay', delayNum);
				}
			}

		});

		$.each($('.anim-content, .inview'),function(){
			$(this).on('inview',function(event,visible){
				var _this = $(this),
				_delay = 1000;

				if (visible == true) {
					if(!_this.hasClass('done')) {
						_this.addClass('visible done');
					} 

					var _animDelay = _this.css('animation-delay').replace('s','');
					if(_animDelay) {
						_delay = _animDelay * 2000;
					}

					setTimeout(function() {
						_this.removeClass('animate anim-content');
					}, _delay);
				}
			});
		});

		$.each($('[anim-control="parent"]'),function(){
			var _this = $(this);

			_this.on('inview', function(event, visible) {
				if (visible == true) {
					if(!$('.anim-content', _this).hasClass('done')) {
						$('.anim-content', _this).addClass('visible done');
					}
				}
			});
		});
	},
	featureAnimation: function(opt) {
		var _feature = {
			target: '',
			baseWidth: 372,
			baseHeight: 362,
			width: 0,
			height: 0,
			delay: 0,
			init: function() {
				_feature.target = document.querySelector(opt.target);
				_feature.delay = _feature.target.getAttribute('anim-delay');
				_feature.update();
				
				window.addEventListener('resize', function() {
					_feature.update();
				});

				_feature.animate('set');

				$(_feature.target).on('inview', function(event,visible){
					if (visible == true) {
						_feature.animate('release');
						$(_feature.target).off('inview');
					}
				});
			},
			update: function() {
				_feature.width = _feature.target.offsetWidth;
				_feature.height = _feature.baseHeight * (_feature.target.offsetWidth / _feature.baseWidth);
				_feature.target.style.height = _feature.height + 'px';
			},
			animate: function(state) {
				_feature.update();

				var _icon1 = _feature.target.querySelector('.icon-1'),
				_icon2 = _feature.target.querySelector('.icon-2'),
				_icon3 = _feature.target.querySelector('.icon-3'),
				_icon4 = _feature.target.querySelector('.icon-4');

				if(state == 'set') {
					TweenMax.set($('> *', _feature.target), {opacity: 0});
				}

				if(state == 'release') {
					TweenMax.set(_icon1, {x: -(_feature.width - _icon1.offsetWidth) / 2, y: -(_feature.height - _icon1.offsetHeight) / 2, scale: 0.8});
					TweenMax.to(_icon1, 0.8, {opacity: 1, scale: 1, delay: _feature.delay, onComplete: function() {
						TweenMax.to(_icon1, 0.5, {opacity: 1, x: 0, y: 0, ease: Power2.easeInOut, delay: 0.5, clearProps: 'all'});

						TweenMax.fromTo(_icon2, 0.5, {opacity: 0, x: _feature.width - (_icon2.offsetWidth), y: 0, scale: 0.8}, {opacity: 1, x: 0, y: 0, scale: 1, ease: Power2.easeInOut, delay: 0.68, clearProps: 'all'});
						TweenMax.fromTo(_icon3, 0.5, {opacity: 0, x: _feature.width - (_icon3.offsetWidth + _icon3.offsetLeft), y: _feature.height - (_icon3.offsetHeight + _icon3.offsetTop), scale: 0.8}, {opacity: 1, x: 0, y: 0, scale: 1, ease: Power2.easeInOut, delay: 0.68, clearProps: 'all'});
						TweenMax.fromTo(_icon4, 0.5, {opacity: 0, x: 0, y: _feature.height - _icon4.offsetHeight, scale: 0.8}, {opacity: 1, x: 0, y: 0, scale: 1, ease: Power2.easeInOut, delay: 0.68, clearProps: 'all'});
					}});
				}
			}
		}
		if(document.querySelector(opt.target)) {
			_feature.init();
		}
	},
	presentationAnimation: function(opt) {
		var _pres = {
			target: '',
			phone: '',
			baseWidth: 688,
			baseHeight: 626,
			width: 0,
			height: 0,
			delay: 0,
			active: false,
			descs: '',
			init: function() {
				_pres.target = document.querySelector(opt.target);
				_pres.phone = _pres.target.querySelector('.hc-pr-phone');
				_pres.descs = _pres.target.querySelector('.hc-pr-desc');
				_pres.update();

				window.addEventListener('load', function() {
					_pres.update();
				});

				window.addEventListener('resize', function() {
					_pres.update();
				});

				_pres.animate('set');

				$(_pres.target).on('inview', function(event,visible) {
					if(visible == true) {
						_pres.active = true;
						_pres.validate();
					}
				});

				window.addEventListener('scroll', function() {
					_pres.validate();
				});


				mimamo.enquire({
					between: 950,
					desktop: function() {
						$(_pres.descs).removeClass('owl-carousel');
						$(_pres.descs).owlCarousel('destroy');
					},
					mobile: function() {
						$(_pres.descs).addClass('owl-carousel');
						$(_pres.descs).owlCarousel({
							items: 2,
							margin: 20,
							responsive: {
								0: {
									items: 1
								},
								500: {
									items: 2
								},
								810: {
									items: 2,
									margin: 15
								}
							}
						});
					}
				});
			},
			validate: function() {
				if (($(_pres.target).offset().top - (window.outerHeight * 0.6)) < window.pageYOffset) {
					if (_pres.active) {
						_pres.animate('release');
						_pres.active = false;
					}
				}
			},
			update: function() {
				_pres.width = _pres.phone.offsetWidth;
				_pres.height = _pres.baseHeight * (_pres.phone.offsetWidth / _pres.baseWidth);
				_pres.phone.style.height = _pres.height + 'px';

				// update the position of the arrows
				var _descSet = _pres.target.querySelectorAll('.hp-de-set'),
				_arrowSet = _pres.target.querySelectorAll('.hp-ar-set');

				for (var i = 0; i < _descSet.length; i++) {
					var _setArrow = $('.hp-ar-set', _pres.target).eq(i);

					if(_setArrow.length == 1) {
						var _arrowPos = _setArrow.attr('arrow-pos').split(',');

						for (var j = 0; j < _arrowPos.length; j++) {
							// top, bottom, left, right

							if (_arrowPos[j] == 'top') {
								var _top = _descSet[i].offsetTop,
								_headerHeight = ((_descSet[i].querySelector('h4').offsetHeight) / 2) - 6;

								_arrowSet[i].style.top = (_top + _headerHeight) + 'px';
							}

							if (_arrowPos[j] == 'right') {
								var _right = _descSet[i].offsetWidth;

								_arrowSet[i].style.right = (_right + 15) + 'px';
							}

							if (_arrowPos[j] == 'bottom') {
								var top = _descSet[i].offsetTop - _arrowSet[i].offsetHeight,
								_headerHeight = (_descSet[i].querySelector('h4').offsetHeight / 2) + 6;

								_arrowSet[i].style.top = (top + _headerHeight) + 'px';
							}

							if (_arrowPos[j] == 'left') {
								var _left = _descSet[i].querySelector('h4').offsetWidth;

								_arrowSet[i].style.left = (_left + 15) + 'px';
							}
						}
					}
				}
			},
			animate: function(state) {
				_pres.update();

				if(state == 'set') {
					TweenMax.set($('.hp-ph-top', _pres.target), {opacity: 0, y: 40});
					TweenMax.set($('.hp-ph-bottom', _pres.target), {opacity: 0, y: -40});

					if(mimamo.width < 950) {
						TweenMax.set($('.hp-de-set', _pres.target), {opacity: 0, y: 40});
					} else {
						TweenMax.set($('.hp-de-set > *', _pres.target), {opacity: 0, y: 40});
					}
				}

				if(state == 'release') {
					TweenMax.to($('.hp-ph-top', _pres.target), 0.5, {opacity: 1, y: 0, delay: 1.5});
					TweenMax.to($('.hp-ph-bottom', _pres.target), 0.5, {opacity: 1, y: 0, delay: 1.5, onComplete: function() {
						$('.hc-pr-arrows', _pres.target).addClass('arrows-animate');

						if(mimamo.width < 950) {
							TweenMax.staggerTo($('.hp-de-set', _pres.target), 0.7, {opacity: 1, y: 0, ease: Power2.out}, 0.2);
						} else {
							$('.hp-de-set', _pres.target).each(function() {
								var _this = $(this);

								TweenMax.staggerTo($('> *', _this), 0.7, {opacity: 1, y: 0, ease: Power2.out, delay: 0.8}, 0.2);
							});
						}
					}});
				}
			}
		}
		if(document.querySelector(opt.target)) {
			_pres.init();
		}
	},
	video: function(opt) {
		var _video = {
			target: '',
			id: '',
			iframe: '',
			parentWidth: 0,
			parentHeight: 0,
			width: 0,
			height: 0,
			init: function() {
				_video.defaults();
				_video.create();
				_video.resize();

				window.addEventListener('resize', function() {
					_video.resize();
				});

				window.addEventListener('load', function() {
					_video.resize();
				});
			},
			defaults: function() {
				_video.target = document.getElementById(opt.target);
				_video.id = opt.id;
			},
			create: function() {
				var _id = 'yt' + opt.index + _video.id;
				_video.iframe = document.createElement('div');
				_video.iframe.setAttribute('id', _id);
				_video.target.append(_video.iframe);

				var player = new YT.Player(_id, {
					width: '100%',
					height: '100%',
					videoId: _video.id,
					playerVars: {
						autoplay: 1,
						rel: 0, 
						showinfo: 0,
						controls: 0, 
						autohide: 1, 
						wmode: 'opaque',
						loop: 1,
						enablejsapi: 1
					},
					events: {
						onReady: function(event) {
							event.target.mute();
							event.target.setVolume(0);
							event.target.setPlaybackQuality('hd1080');
						},
						onStateChange: function(event) {
							if (event.data == YT.PlayerState.BUFFERING) {
								event.target.setPlaybackQuality('hd1080');
							}
							if(event.data === YT.PlayerState.ENDED){
								event.target.seekTo(0);
							}
						}
					}
				});
				_video.iframe = document.getElementById(_id);				
			},
			resize: function() {
				_video.width  = _video.iframe.offsetWidth;
				_video.height = _video.iframe.offsetHeight;
				_video.parentWidth = _video.target.offsetWidth;
				_video.parentHeight = _video.target.offsetHeight;

				var _newWidth = 0,
				_newHeight = 0;

				if(_video.width/_video.parentWidth < _video.height/_video.parentHeight) {
					_newWidth  = _video.parentWidth * 2;
					_newHeight = _newWidth / _video.width * _video.height;
				} else {
					_newHeight = _video.parentHeight * 2;
					_newWidth  = _newHeight / _video.height * _video.width;
				}

				var _marginTop  = (_video.parentHeight - _newHeight) / 2,
				_marginLeft = (_video.parentWidth  - _newWidth ) / 2;

				$(_video.iframe).css({marginTop: _marginTop, marginLeft: _marginLeft, height: _newHeight, width: _newWidth});
			}
		}
		if(!mimamo.isMobile) {
			_video.init();
		}
	},
	selectBox: function(opt) {
		var _select = {
			target: '',
			box: '',
			title: '',
			list: '',
			lis: '',
			current: 0,
			init: function() {
				_select.defaults();
				_select.create();
				_select.clicks();
			},
			defaults: function() {
				_select.target = document.querySelector(opt.target);
				_select.box = _select.target.querySelector('select');

				// if mobile, use native select box
				if(mimamo.isMobile && window.innerWidth < 1020) {
					_select.target.classList.add('select-mobile');
				} else {
					_select.box.style.display = "none";
				}
			},
			create: function() {
				// create title
				_select.title = document.createElement('div');
				_select.title.classList.add('com__sb-title');

				// create list
				_select.list = document.createElement('ul');
				_select.list.classList.add('com__sb-list');

				var _options = _select.box.querySelectorAll('option');
				for (var i = 0; i < _options.length; i++) {
					var _entry = document.createElement('li');
					_entry.setAttribute('select-number', i);

					// target the selected option
					if(_options[i].selected) {
						_select.current = i;
						_entry.classList.add('selected');
					}

					// put the text
					_entry.innerHTML = _options[i].textContent;

					// put the entry to list
					_select.list.appendChild(_entry);
				}

				// append the containers
				_select.target.appendChild(_select.title);
				_select.target.appendChild(_select.list);
			},
			clicks: function() {
				// when clicking title
				_select.title.addEventListener('click', function() {
					_select.target.classList.toggle('selected');
				});

				// when click lists
				_select.lis = _select.list.querySelectorAll('li');
				for (var i = 0; i < _select.lis.length; i++) {
					_select.lis[i].addEventListener('click', function() {
						var _num = this.getAttribute('select-number');

						_select.update(_num);
						_select.close();
					});
				}

				// trigger the current select option
				_select.lis[_select.current].click();

				// for native select box
				_select.box.addEventListener('change', function() {
					var _num = _select.box.options.selectedIndex;

					_select.update(_num);
					_select.close();
				});

				// when click outside the checkbox
				window.addEventListener('click', function(e){   
					if (!(document.querySelector(opt.target).contains(e.target))){
						_select.close();
					}
				});
			},
			update: function(number) {
				// put class on the selected li
				for (var i = 0; i < _select.lis.length; i++) {
					_select.lis[i].classList.remove('selected');
				}
				_select.lis[number].classList.add('selected');

				// make active the selectec option
				_select.box.options.selected = false;
				_select.box.options[number].selected = true;

				// update the text of the titles
				_select.title.innerHTML = '';
				_select.title.innerHTML = _select.lis[number].textContent;
			},
			close: function() {
				_select.target.classList.remove('selected');
			}
		}
		_select.init();
	},
	oneNav: function(opt) {
		var _nav = {
			target: '',
			ids: [],
			init: function() {
				_nav.target = document.querySelector(opt.target);

				if($(_nav.target).length == 1) {
					$('li', _nav.target).eq(0).addClass('active');

					$(_nav.target).onePageNav({
						currentClass: 'active',
						scrollSpeed: 1000,
						filter: ':not(.external)'
					});

					for (var i = 0; i < $('a', _nav.target).length; i++) {
						_nav.ids.push($('a', _nav.target).eq(i).attr('href'));
					}

					for (var i = 0; i < _nav.ids.length; i++) {
						if(_nav.ids[i] == mimamo.hash) {
							var _id = document.querySelector(_nav.ids[i]);

							$('body, html').animate({scrollTop:$(_id).offset().top - ($('header').outerHeight())}, 1000); 
						}
					}
				}
			}
		}
		_nav.init();
	},
	enquire: function(opt) {
		var _enquire = {
			status: opt.update,
			desktop: opt.desktop,
			mobile: opt.mobile,
			counterDesktop: 0,
			counterMobile: 0,
			width: 0,
			between: 767,
			init: function() {
				if(opt.between != null) {
					_enquire.between = opt.between;
				}

				window.addEventListener('resize', function() {
					_enquire.update();
				});
				_enquire.update();
			},
			update: function() {
				_enquire.width = $(window).outerWidth();

				if(_enquire.status == 'always') {
					// update every resize
					if(_enquire.width > _enquire.between) {
						_enquire.desktop();
					} else {
						_enquire.mobile();
					}
				} else {
					// if detected desktop or mobile then update once
					if(_enquire.width > _enquire.between) {
						_enquire.counterMobile = 0;

						// if desktop
						if(_enquire.counterDesktop == 0) {
							_enquire.counterDesktop = 1;
							_enquire.desktop();
						}
					} else {
						_enquire.counterDesktop = 0;

						// if mobile
						if(_enquire.counterMobile == 0) {
							_enquire.counterMobile = 1;
							_enquire.mobile();
						}
					}
				}
			}
		}
		_enquire.init();
	},
	mobile: function() {
		var _mobile = {
			main: '',
			mainContent: '',
			cover: '',
			headerHeight: 0,
			bar: '',
			itemDesktop: '',
			itemMobile: '',
			itemContent: '',
			state: true,
			init: function() {
				_mobile.main = document.querySelector('.menu-mobile.main');
				_mobile.mainContent = _mobile.main.querySelector('.me-mo-main');
				_mobile.cover = document.querySelector('.menu-mobile.cover');
				_mobile.bar = document.querySelector('.menu-bar');

				_mobile.itemDesktop = document.querySelector('[menu-item-desktop]');
				_mobile.itemMobile = document.querySelector('[menu-item-mobile]');
				_mobile.itemContent = document.querySelector('[menu-item-content]');

				mimamo.enquire({
					between: 950,
					desktop: function() {
						$(_mobile.itemDesktop).append(_mobile.itemContent);
						TweenMax.set($('li', _mobile.itemContent), {clearProps: 'all'});
					},
					mobile: function() {
						$(_mobile.itemMobile).append(_mobile.itemContent);
					}
				});

				_mobile.resize();

				window.addEventListener('load', function() {
					_mobile.resize();					
				});

				window.addEventListener('resize', function() {
					_mobile.resize();
				});

				$(_mobile.bar).click(function() {
					var _this = $(this);

					if(_mobile.state == true) {
						if(_this.hasClass('active')) {
							_mobile.hide();
						} else {
							_mobile.show();
						}
					}
				});

				$('a', _mobile.itemContent).click(function() {
					_mobile.hide();
				});
			},
			resize: function() {
				_mobile.headerHeight = $('header').outerHeight(),
				_marginTop = ($(_mobile.main).outerHeight() - $(_mobile.mainContent).outerHeight()) / 2;
				if (_marginTop < 0) {
					_marginTop = 0;
				} 

				$(_mobile.main).css({top: _mobile.headerHeight, height: ($(window).outerHeight() - _mobile.headerHeight)});
				$(_mobile.mainContent).css({marginTop: _marginTop});
			},
			show: function() {
				_mobile.state = false;

				$('body').addClass('menu-active');
				$(_mobile.bar).addClass('active');

				$(_mobile.main).show();
				TweenMax.set(_mobile.main, {opacity: 1});
				_mobile.resize();

				TweenMax.set($('li', _mobile.mainContent), {clearProps: 'all'});
				TweenMax.set($('li', _mobile.mainContent), {opacity: 0, y: -40});
				TweenMax.staggerTo($('li', _mobile.mainContent), 0.4, {opacity: 1, y: 0, clearProps: 'all'}, 0.05, function() {
					_mobile.state = true;
				});
			},
			hide: function() {
				_mobile.state = false;

				$('body').removeClass('menu-active');
				$(_mobile.bar).removeClass('active');
				
				TweenMax.to(_mobile.main, 0.4, {opacity: 0, onComplete: function() {
					$(_mobile.main).hide();
				}});
				TweenMax.staggerTo($('li', _mobile.mainContent), 0.4, {opacity: 0, y: -40, onComplete: function() {
					_mobile.state = true;
				}}, 0.1);
			}
		}
		_mobile.init();
	}
}
mimamo.init();

$(window).resize(function() {
	mimamo.resize();
	clearTimeout(mimamo.resizeTimer);
	mimamo.resizeTimer = setTimeout(function() {
		mimamo.resize();
	}, 400);
});

window.onYouTubeIframeAPIReady = function() {
	$('.he-co-vid').each(function(index) {
		var _this = $(this),
		_id = 'yt' + index;

		_this.attr('id', _id);
		mimamo.video({
			index: index,
			target: _id,
			id: _this.attr('yt-id')
		});
	});
}